<?php
  require_once "engine/init.php";
	
  $lang = new Lang();
  require_once $lang->detect();

  $engine = new Engine();
  
  require_once $engine -> initPage();
?>

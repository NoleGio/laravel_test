$(document).ready(function(){

	//var headerLogo = $('.js-header-logo');
	//headerLogo.css({'left':($('.header-content').width()-368)/2});

	$('.js-full-height').height($(window).height());

	$('.js-nav-btn').on('click',function(){
		$('.header-menu').toggle();
	});

	$(window).on('scroll',function(){
		if($(window).scrollTop()>100)
			$('.js-header').addClass('scrolled');
		else
			$('.js-header').removeClass('scrolled');
	});

	$('.js-self-active').on('click',function(){
		var that = $(this);
		if( that.hasClass('active') ){
			that.removeClass('active');
		}else{
			$('.js-self-active.active').removeClass('active');
			that.addClass('active');
		}
	});

	$('#contact-form').on('submit',function(event){
		$.ajax({
			url: '/ajax/_cov9adw.php',
			type: 'POST',
			data: $(this).serialize(),
			success: function(data) {
				$('#alertbox').html(data);
			},
			error: function() {
				$('#alertbox').html('<div class="text-red">წერილის გაგზავნა ვერ მოხერხდა.</div>');
			}
		});
		event.preventDefault();
	});

	function calc() {
		var val = parseFloat($('.js-room.available[data-for="'+$('#calc-area').val()+'"]').attr('data-square'));
		var area = parseInt($('#calc-type').val());
		$('#calc-target').text(Math.round(val*area));
	}

	$('#calc-area').on('change',function(){
		$('.js-room.available.active').removeClass('active');
		$('.js-room.available[data-for="'+$(this).val()+'"]').addClass('active');
		calc();
	});
	$('#calc-type').on('change',function(){
		calc();
	});

	if( $('body').hasClass('js-counter') ) {
		$(document).on('scroll',function(){
			yPos = $('#counting-stats').offset().top;
			wHeight = $(window).height();
			if( $(window).scrollTop()+wHeight > yPos ) {
				var f = $('.js-numerator.first');
				var s = $('.js-numerator.second');
				var t = $('.js-numerator.third');
				f.numerator({toValue:parseInt(f.attr('data-count')),duration:500});
				s.numerator({toValue:parseInt(s.attr('data-count')),duration:2000});
				t.numerator({toValue:parseInt(t.attr('data-count')),duration:2000});
				$(document).off('scroll');
			}
		});
	}
	// if( $('body').hasClass('js-progress') ) {
	// 	$(document).on('scroll',function(){
	// 		var yPos = $('#progressive').offset().top;
	// 		var target = $('#build-number');
	// 		var progress = parseInt(target.attr('data-progress'));
	// 		var wHeight = $(window).height();
	// 		if( $(window).scrollTop()+wHeight > yPos ) {
	// 			target.numerator({toValue:progress,duration:2000,onStart:function(){$('#build-progress').css({'width':progress+'%'})}});
	// 			$(document).off('scroll');
	// 		}
	// 	});
	// }

	$('.js-project').on('mouseover',function(){
		var that = $(this);
		var collage = that.parent().next();
		collage.css({'background-image':'url('+that.attr('data-src')+')'});
	});

	$('.js-floor').on('mouseover',function(){
		$('.floor.active').removeClass('active');
		var val = parseInt($(this).attr('data-floor'));

		var floor = $('#floor');
		var curr = parseInt(floor.text());

		var diff = Math.abs( val - curr );
		floor.numerator({toValue: val ,duration: 50*diff});
	});

	$('.js-elevator-btn').on('click',function(){
		var floor = $('#floor');
		var curr = parseInt(floor.text());
		var that = $(this);
		if( that.attr('data-direction') == 'up' && curr < 15 ) {
			curr++;
		}	else if( that.attr('data-direction') == 'down' && curr > 1 ) {
			curr--;
		}
		floor.text(curr);
		$('.floor.active').removeClass('active');
		$('.floor[data-floor=\''+curr+'\']').addClass('active');
	});

	$.each($('.js-room.available'),function(){
		var that = $(this);
		var topPos = that.offset().top + that.height() / 2 - 43;
		var leftPos = that.offset().left + that.width() / 2 - 43;

		$('.sv-floor').prepend('<a href="'+that.parent().attr('xlink:href')+'" class="active sv-title js-title animated zoomIn" data-after="'+that.attr('data-for')+'" style="top:'+topPos+'px;left:'+leftPos+'px;"><div class="sv-title-cover"></div><div class="sv-title-text"><p>ბინა '+that.attr('data-for')+'</p><p>'+that.attr('data-square')+'მ<sup>2</sup></p></div></a>');
	});

	$.each($('.js-room.sold'),function(){
		var that = $(this);
		var topPos = that.offset().top + that.height() / 2 - 22;
		var leftPos = that.offset().left + that.width() / 2 - 67.75;

		$('.sv-floor').prepend('<div class="sv-sold" style="top:'+topPos+'px;left:'+leftPos+'px;">გაყიდულია</div>');
	});

	$.each($('.js-room.reserved'),function(){
		var that = $(this);
		var topPos = that.offset().top + that.height() / 2 - 22;
		var leftPos = that.offset().left + that.width() / 2 - 76.5;

		$('.sv-floor').prepend('<div class="sv-sold" style="top:'+topPos+'px;left:'+leftPos+'px;">დაჯავშნულია</div>');
	});

	$('.js-room.available').on('mouseover',function(){
		var that = $(this);
		that.addClass('active');

		// $('#calc-area').val(parseFloat(that.attr('data-square')));
		// calc();
		// $('.js-title[data-after="'+that.attr('data-for')+'"]').addClass('active');
	});
	$('.js-room.available').on('mouseout',function(){
		// $('.sv-title.active').removeClass('active');
		$('.js-room.active').removeClass('active');
	});
	$('.js-title').on('mouseover',function(){
		var that = $(this);
		// that.addClass('active');
		$('.js-room[data-for="'+that.attr('data-after')+'"]').addClass('active');
	});

	new WOW().init();
});

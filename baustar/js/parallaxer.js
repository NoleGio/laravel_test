var Parallaxer = {
  init: function() {
    $(window).scroll(this.setCoords);
  },

  setCoords: function parallax(e) {
    var els = Array.prototype.slice.call($('.parallax'));
    var ST = $(window).scrollTop();

    els.forEach(function(el) {
      var pObj = $(el);
      var data = pObj.data();
      var POH = pObj.height();
      var POW = pObj.width();
      var PORH = data.height || POH;
      var PORW = data.width || POW;
      var offset = pObj.offset();
      var start = offset.top - $(window).height();
      var end = offset.top + POH;

      var pos = Math.max(Math.min(ST, end) - start, 0);
      var l = end - start;

      var scale = data.scale || l / POH;

      var ratio = PORW / POW;
      var IMGH = PORH / ratio;
      var deltaP = data.deltap;
      var delta = (IMGH - POH) / 100 * deltaP;
      var changeProp = data.changeprop || 'backgroundPosition';
      var origPos = data.origpos || 0;
      var dir = data.dir || 1;

      var bgPos = Math.round((pos - (l / 2)) / scale) - parseInt(delta);

      if(changeProp == 'backgroundPosition') {
        pObj.css({ backgroundPosition: 'center ' + bgPos + 'px' });
      } else {
        pObj.css({ top: (dir * bgPos) + parseInt(origPos) + 'px' });
      }
    });
  }
};

$(function() {
  Parallaxer.init();
})

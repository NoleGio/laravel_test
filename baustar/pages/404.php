<?php //header("HTTP/1.0 404 Not Found"); ?>
<!-- BAU Star website designed by Bunny.ge -->
<!DOCTYPE html>
<html>
<head>
	<?php require_once '_blocks/head.php'; ?>
</head>
<body class="animated fadeIn">
	<section id="header">
		<div class="scrolled header">
			<div class="wide container">
				<div class="header-content clearfix">
						<a href="/" class="header-logo"><img src="img/baustar-h.png" alt=""></a>
						<ul class="header-menu clearfix animated fadeIn">
							<?php require_once '_blocks/header-menu.php'; ?>
						</ul>
						<div class="nav-btn js-nav-btn animated fadeIn"><span></span><span></span><span></span><span></span></div>
				</div>
			</div>
		</div>
	</section>

	<section id="page404">
		<div class="heading js-full-height">
			<div class="fs-table">
				<div class="fs-row">
					<div class="fs-col">
						<div class="fs-item">
							<div class="err404 animated fadeIn">
								<h1><?php echo $word['_w45']; ?> 404</h1>
								<p><?php echo $word['_w46']; ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<?php require_once '_blocks/footer.php'; ?>

	<?php require_once '_blocks/scripts.php'; ?>
</body>
</html>

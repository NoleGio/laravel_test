<!-- BAU Star website designed by Bunny.ge -->
<!DOCTYPE html>
<html>
<head>
	<?php require_once '_blocks/head.php'; ?>
</head>
<body class="animated fadeIn fixed-heading">
	<section id="header">
		<div class="scrolled header">
			<div class="wide container">
				<div class="header-content clearfix">
						<a href="/" class="header-logo"><img src="/img/baustar-h.png" alt=""></a>
						<ul class="header-menu clearfix animated fadeIn">
							<?php require_once '_blocks/header-menu.php'; ?>
						</ul>
						<div class="nav-btn js-nav-btn animated fadeIn"><span></span><span></span><span></span><span></span></div>
				</div>
			</div>
		</div>
	</section>

	<section id="heading">
		<div class="heading parallax" data-width="1600" data-height="900" data-deltap="50">
			<div class="counters-blur">
				<div class="block-t">
					<div class="wide container">
						<div class="block-title">
							<h1 class="animated fadeInRight">Bau Star <?php echo $word['_w4']; ?> 2</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="project">
		<div class="block">
			<div class="wide container">
				<img src="/img/v12.jpg" class="responsive img" alt="varketili-2">

				<!-- <div class="boxer bo"></div> -->

			<h1 class="wow fadeInUp text-center"><?php echo $word['_w55']; ?></h1>
			<div class="divider"></div>

			<div class="row margin-top-20 js-lightbox">

				<div class="col md-4">
					<div class="gallery-item wow fadeIn" href="/img/v12.jpg">
						<div class="galley-item-inner">
							<img src="/img/v12.jpg" class="responsive img gallery-item-img" alt="">
							<div class="gallery-item-cover">
								<div class="fs-table">
									<div class="fs-row">
										<div class="fs-col">
											<div class="fs-item">
												<i class="fa fa-search-plus fa-lg"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col md-4">
					<div class="gallery-item wow fadeIn" href="/img/v11.jpg" data-wow-delay=".5s">
						<div class="galley-item-inner">
							<img src="/img/v11.jpg" class="responsive img gallery-item-img" alt="">
							<div class="gallery-item-cover">
								<div class="fs-table">
									<div class="fs-row">
										<div class="fs-col">
											<div class="fs-item">
												<i class="fa fa-search-plus fa-lg"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col md-4">
					<div class="gallery-item wow fadeIn" href="/img/v13.jpg" data-wow-delay="1s">
						<div class="galley-item-inner">
							<img src="/img/v13.jpg" class="responsive img gallery-item-img" alt="">
							<div class="gallery-item-cover">
								<div class="fs-table">
									<div class="fs-row">
										<div class="fs-col">
											<div class="fs-item">
												<i class="fa fa-search-plus fa-lg"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div> <!-- row -->

		</div> <!-- wide container -->
	</section>

	<h1 class="wow fadeInUp text-center"><?php echo $word['_w56']; ?></h1>
	<div class="divider"></div>

	<section id="location"><div id="map" class="google-map" data-lat="41.690966" data-lng="44.867451" data-icon="/img/map-2.png"></div></section>

	<?php require_once '_blocks/contact.php'; ?>

	<?php require_once '_blocks/footer.php'; ?>

	<?php require_once '_blocks/scripts.php'; ?>
</body>
</html>

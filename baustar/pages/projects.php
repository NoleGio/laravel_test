<!-- BAU Star website designed by Bunny.ge -->
<!DOCTYPE html>
<html>
<head>
	<?php require_once '_blocks/head.php'; ?>
</head>
<body class="animated fadeIn fixed-heading">
	<section id="header">
		<div class="header scrolled">
			<div class="wide container">
				<div class="header-content clearfix">
						<a href="/" class="header-logo js-header-logo"><img src="/img/baustar-h.png" alt="baustar"></a>
						<ul class="header-menu clearfix animated fadeIn">
							<?php require_once '_blocks/header-menu.php'; ?>
						</ul>
						<div class="nav-btn js-nav-btn animated fadeIn"><span></span><span></span><span></span><span></span></div>
				</div>
			</div>
		</div>
	</section>

	<section id="projects">
		<div class="block">
			<div class="wide container">

				<h1 class="wow fadeInUp text-center"><?php echo $word['_w57']; ?></h1>
				<div class="divider"></div>

				<div class="row none">
					<div class="project-item clearfix">
						<div class="project-headline-bg"></div>
						<div class="col md--60 wow slideInLeft">
							<a class="boxer project-boxer" href="/digomi-green-town">
								<div class="project-headline"></div>
								<div class="project-img">
									<p class="project-tooltip"><?php echo $word['_w6']; ?></p>
									<div class="project-gradient">
										<div href="#" class="button" style="position:absolute;bottom:0;left:0;z-index:10;"><?php echo $word['_w7']; ?></div>
									</div>
									<div class="project-picker">
										<div class="project-img-item js-project" data-src="img/house.jpg"><img src="img/house.jpg" alt=""></div>
										<div class="project-img-item js-project delay-1" data-src="img/slider/images/2.jpg"><img src="img/slider/images/2.jpg" alt=""></div>
										<div class="project-img-item js-project delay-2" data-src="img/digomi-2.jpg"><img src="img/digomi-2.jpg" alt=""></div>
									</div>
									<div class="project-collage" style="background-image:url('/img/house.jpg');"></div>
								</div>
							</a>
						</div><!-- col md-60 -->
						<div class="col md--40">
							<div class="project-info">
								<div class="project-title">
									<h2 class="wow fadeInRight"><i class="fa fa-home"></i> <?php echo $word['_w3']; ?></h2>
								</div>
								<ul class="list project-list">
									<li class="wow fadeInDown" data-wow-delay="0.5s"><i class="fa fa-map-marker"></i> <?php echo $word['_w8']; ?></li>
									<li class="wow fadeInDown" data-wow-delay="1s"><i class="fa fa-hourglass"></i> <?php echo $word['_w9']; ?></li>
								</ul>
							</div>
						</div><!-- col md-40 -->
					</div>
				</div><!--row-->

				<h1 class="wow fadeInUp text-center"><?php echo $word['_w58']; ?></h1>
				<div class="divider"></div>

				<div class="row none">
					<div class="project-item alternative clearfix">
						<div class="project-headline-bg"></div>
						<div class="col md--60 wow slideInRight">
							<a class="boxer project-boxer" href="/varketili-2">
								<div class="project-headline"></div>
								<div class="project-img">

									<p class="project-tooltip red"><?php echo $word['_w10_']; ?></p>
									<div class="project-gradient"></div>
									<div class="project-picker">
										<div class="project-img-item js-project" data-wow-delay="0.5s" data-src="img/v12.jpg"><img src="img/v12.jpg" alt=""></div>
										<div class="project-img-item js-project" data-wow-delay="1s" data-src="img/v11.jpg"><img src="img/v11.jpg" alt=""></div>
										<div class="project-img-item js-project" data-wow-delay="1.5s" data-src="img/v13.jpg"><img src="img/v13.jpg" alt=""></div>
									</div>
									<div class="project-collage" style="background-image:url('/img/v12.jpg');"></div>

								</div>
							</a>
						</div><!-- col md-60 -->

						<div class="col md--40">
							<div class="project-info">
								<div class="project-title">
									<h2 class="wow fadeInLeft">BAU STAR <?php echo $word['_w4']; ?> 2 <i class="fa fa-home"></i></h2>
								</div>

								<ul class="list project-list">
									<li class="wow fadeInDown a-delay-1"><?php echo $word['_w11']; ?> <i class="fa fa-map-marker"></i></li>
									<li class="wow fadeInDown a-delay-2"><?php echo $word['_w9']; ?> <i class="fa fa-hourglass"></i></li>
									<li class="wow fadeInDown a-delay-2"><?php echo $word['_w10']; ?> <i class="fa fa-tags"></i></li>
								</ul>
							</div>
						</div><!-- col md-40 -->
					</div>
				</div><!--row-->

				<div class="row none">
					<div class="project-item clearfix">
						<div class="project-headline-bg"></div>
						<div class="col md--60 wow slideInLeft">
							<a class="boxer project-boxer" href="/varketili-1">
								<div class="project-headline"></div>
								<div class="project-img">
									<p class="project-tooltip red"><?php echo $word['_w10_']; ?></p>
									<div class="project-collage" style="background-image:url('/img/13.jpg');"></div>
								</div>
							</a>
						</div><!-- col md-60 -->
						<div class="col md--40">
							<div class="project-info">
								<div class="project-title">
									<h2 class="wow fadeInRight"><i class="fa fa-home"></i> BAU STAR <?php echo $word['_w4']; ?> 1</h2>
								</div>
								<ul class="list project-list">
									<li class="wow fadeInDown" data-wow-delay="0.5s"><i class="fa fa-map-marker"></i> <?php echo $word['_w12']; ?></li>
									<li class="wow fadeInDown" data-wow-delay="1s"><i class="fa fa-hourglass"></i> <?php echo $word['_w13']; ?></li>
									<li class="wow fadeInDown" data-wow-delay="1.5s"><i class="fa fa-tags"></i> <?php echo $word['_w10']; ?></li>
								</ul>
							</div>
						</div><!-- col md-40 -->
					</div>
				</div><!--row-->

			</div>
		</div>
	</section>

	<?php require_once '_blocks/phone.php'; ?>

	<?php require_once '_blocks/contact.php'; ?>

	<section id="location"><div id="map" class="google-map" data-lat="41.762529" data-lng="44.776768" data-icon=""></div></section>

	<?php require_once '_blocks/footer.php'; ?>

	<?php require_once '_blocks/scripts.php'; ?>

</body>
</html>

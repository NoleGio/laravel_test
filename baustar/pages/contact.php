<!-- BAU Star website designed by Bunny.ge -->
<!DOCTYPE html>
<html>
<head>
	<?php require_once '_blocks/head.php'; ?>
</head>
<body class="animated fadeIn fixed-heading">
	<section id="header">
		<div class="scrolled header">
			<div class="wide container">
				<div class="header-content clearfix">
						<a href="/" class="header-logo js-header-logo"><img src="img/baustar-h.png" alt=""></a>
						<ul class="header-menu clearfix animated fadeIn">
							<?php require_once '_blocks/header-menu.php'; ?>
						</ul>
						<div class="nav-btn js-nav-btn animated fadeIn"><span></span><span></span><span></span><span></span></div>
				</div>
			</div>
		</div>
	</section>

	<section id="heading">
		<div class="heading parallax" data-width="1600" data-height="900" data-deltap="50">
			<div class="counters-blur">
				<div class="block-t">
					<div class="wide container">
						<div class="block-title">
							<h1 class="animated fadeInRight"><?php echo $word['_w50']; ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php require_once '_blocks/contact.php'; ?>

	<section id="location"><div id="map" class="google-map" data-lat="41.762529" data-lng="44.776768" data-icon=""></div></section>

	<?php require_once '_blocks/footer.php'; ?>

	<?php require_once '_blocks/scripts.php'; ?>
</body>
</html>

<div class="margin-bottom-20 clearfix">
  <div class="calc calc-right calc-floors js-self-active">
    <span><?php echo $word['_w15']; ?> <strong class="text-green"><?php echo $flat; ?></strong> <i class="fa fa-caret-down"></i></span>
    <ul class="floor-dropdown clearfix">
      <?php
        $q = $mysqli->query("SELECT id FROM flat WHERE floor={$floor} AND available = 1;");
        while ($s = $q->fetch_assoc()) {
          echo "<li><a href=\"/digomi-green-town/{$floor}/{$s['id']}\">{$s['id']}</a></li>";
        }
       ?>
    </ul>
  </div>
  <div class="calc calc-right calc-floors js-self-active">
    <span><?php echo $word['_w26']; ?> <strong class="text-green"><?php echo $floor; ?></strong> <i class="fa fa-caret-down"></i></span>
    <ul class="floor-dropdown clearfix">
      <?php
        $q = $mysqli->query("SELECT id FROM floor WHERE flats > 0;");
        while ($s = $q->fetch_assoc()) {
          echo "<li><a href=\"/digomi-green-town/{$s['id']}\">{$s['id']}</a></li>";
        }
       ?>
    </ul>
  </div>
</div>

<div class="boxer margin-bottom-20">
  <div class="row">
    <div class="col md-6">
      <ul class="list">
        <li><b><?php echo $word['_w69']; ?> N<?php echo $flat; ?></b></li>
        <li><b><?php echo $word['_w27']; ?></b>: <?php echo $f[1]['area']; ?> მ<sup>2</sup></li>
        <?php
         if( !empty($f[0][0]) ) {
            if( strpos($f[0][0],';')===false ) {
              echo "<li><b>{$word['_w63']}:</b> {$f[0][3]} მ<sup>2</sup></li>";
            }else{
              $_r = explode(';',$f[0][0]);
              $_s = sizeof($_r);
              for($i=1;$i<=$_s;$i++) {
                echo "<li><b>{$word['_w63']} {$i}:</b> {$_r[$i-1]} მ<sup>2</sup></li>";
              }
            }
          }
          echo (!empty($f[0][1]))?"<li><b>{$word['_w64']}:</b> {$f[0][1]} მ<sup>2</sup></li>":"";
          echo (!empty($f[0][2]))?"<li><b>{$word['_w65']}:</b> {$f[0][2]} მ<sup>2</sup></li>":"";
          if( !empty($f[0][3]) ) {
            if( strpos($f[0][3],';')===false ) {
              echo "<li><b>{$word['_w66']}:</b> {$f[0][3]} მ<sup>2</sup></li>";
            }else{
              $_r = explode(';',$f[0][3]);
              $_s = sizeof($_r);
              for($i=1;$i<=$_s;$i++) {
                echo "<li><b>{$word['_w66']} {$i}:</b> {$_r[$i-1]} მ<sup>2</sup></li>";
              }
            }
          }
          echo (!empty($f[0][4]))?"<li><b>{$word['_w67']}:</b> {$f[0][4]} მ<sup>2</sup></li>":"";
          if( !empty($f[0][5]) ) {
            if( strpos($f[0][5],';')===false ) {
              echo "<li><b>{$word['_w68']}:</b> {$f[0][5]} მ<sup>2</sup></li>";
            }else{
              $_r = explode(';',$f[0][5]);
              $_s = sizeof($_r);
              for($i=1;$i<=$_s;$i++) {
                echo "<li><b>{$word['_w68']} {$i}:</b> {$_r[$i-1]} მ<sup>2</sup></li>";
              }
            }
          }
        ?>
      </ul>
    </div>
    <div class="col md-6">
      <img src="<?php echo $f[1]['img']; ?>" class="responsive img" alt="flat">
    </div>
  </div>
</div>

<div class="clearfix  margin-bottom-30">
  <a href="/plan/<?php echo $floor; ?>.pdf" class="calc calc-right" style="cursor:pointer;" target="blank"><i class="fa fa-file-pdf-o"></i> <?php echo $word['_w61']; ?></a>
</div>

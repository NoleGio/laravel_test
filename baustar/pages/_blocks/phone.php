<section id="phone">
  <div class="phone counters parallax" data-width="1110" data-height="360" data-deltap="50">
    <div class="counters-blur">
      <div class="block">
        <div class="wide container text-center">
          <div class="phone-box clearfix wow flipInY">
            <img src="img/phone.png" class="phone-img animated tada" alt="phone">
            <div class="phone-number"><p>322 24 24 80</p></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

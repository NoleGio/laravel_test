<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<?php if( Engine::$slashes[0] == 'home' ) { ?>
<script type="text/javascript" src="/js/wowslider.js?v=2"></script>
<?php } ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" integrity="sha256-z6FznuNG1jo9PP3/jBjL6P3tvLMtSwiVAowZPOgo56U=" crossorigin="anonymous"></script>
<script type="text/javascript" src="/js/parallaxer.js"></script>
<script type="text/javascript" src="/js/jquery-numerator.js"></script>
<?php  if( Engine::$slashes[0] == 'varketili-2' || Engine::$slashes[0] == 'digomi-green-town'|| Engine::$slashes[0] == 'gallery' ) { ?>
<script type="text/javascript" src="/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript">
$('.js-lightbox').magnificPopup({
  delegate: '.gallery-item',
  type: 'image',
  gallery:{enabled:true}
});
</script>
<?php } ?>
<script type="text/javascript" src="/js/script.js?v=3"></script>
<!-- facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=103008246746943";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
<!-- chatra -->
<script>(function(d, w, c) {w.ChatraID = 'tzhDwffQpZFZyY3qD';var s = d.createElement('script');w[c] = w[c] || function() {(w[c].q = w[c].q || []).push(arguments);};s.async = true;s.src = (d.location.protocol === 'https:' ? 'https:': 'http:') + '//call.chatra.io/chatra.js';if (d.head) d.head.appendChild(s);})(document, window, 'Chatra');</script>
<!-- google maps api -->
<script>
function initMap() {
  var mapItem = document.getElementById('map');
  var lat = parseFloat(mapItem.getAttribute('data-lat'));
  var lng = parseFloat(mapItem.getAttribute('data-lng'));
  var img = mapItem.getAttribute('data-icon');
  var uluru = {lat: lat, lng: lng};
  var map = new google.maps.Map(mapItem, {zoom: 16,scrollwheel: false,center: uluru});
  var marker = new google.maps.Marker({position: uluru,icon: img,map: map});
  //var silver = [{elementType: 'geometry',stylers: [{color: '#f5f5f5'}]},{elementType: 'labels.text.fill',stylers: [{color: '#616161'}]},{elementType: 'labels.text.stroke',stylers: [{color: '#f5f5f5'}]},{featureType: 'administrative.land_parcel',elementType: 'labels.text.fill',stylers: [{color: '#bdbdbd'}]},{featureType: 'poi',elementType: 'geometry',stylers: [{color: '#eeeeee'}]},{featureType: 'poi',  elementType: 'labels.text.fill',stylers: [{color: '#757575'}]},{featureType: 'poi.park',elementType: 'geometry',stylers: [{color: '#C8E6C9'}]},{featureType: 'poi.park',elementType: 'labels.text.fill',stylers: [{color: '#9e9e9e'}]},{featureType: 'road',elementType: 'geometry',stylers: [{color: '#ffffff'}]},{featureType: 'road.arterial',elementType: 'labels.text.fill',stylers: [{color: '#757575'}]},{featureType: 'road.highway',elementType: 'geometry',stylers: [{color: '#dadada'}]},{featureType: 'road.highway',elementType: 'labels.text.fill',stylers: [{color: '#616161'}]},{featureType: 'road.local',elementType: 'labels.text.fill',stylers: [{color: '#9e9e9e'}]},{featureType: 'transit.line',elementType: 'geometry',stylers: [{color: '#e5e5e5'}]},{featureType: 'transit.station',elementType: 'geometry',stylers: [{color: '#eeeeee'}]},{featureType: 'water',elementType: 'geometry',stylers: [{color: '#BBDEFB'}]},{featureType: 'water',elementType: 'labels.text.fill',stylers: [{color: '#9e9e9e'}]}];
  //map.setOptions({styles: silver});
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiIP1EPLLyucV98xGkDzQMO42_CQtr-64&callback=initMap"></script>

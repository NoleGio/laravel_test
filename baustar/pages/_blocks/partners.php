<section id="partners">
  <div class="partners">
    <div class="block">
      <h1 class="wow fadeInUp text-center"><?php echo $word['_w44']; ?></h1>
      <div class="divider"></div>
      <div class="wide container text-center">
        <a href="http://www.tbcbank.ge" class="partner-item wow zoomIn" target="blank"><img src="img/tbc.png" alt="TBC Bank"></a>
        <a href="http://bankofgeorgia.ge" class="partner-item wow zoomIn" data-wow-delay=".5s" target="blank"><img src="img/bog.png" alt="Bank Of Georgia"></a>
        <a href="http://ge.vtb.com.ge" class="partner-item wow zoomIn" data-wow-delay="1s" target="blank"><img src="img/vtb.png" alt="VTB Bank"></a>
        <a href="http://www.heidelbergcement.com/ge/en/country/plants_and_companies/HeidelbergBeton+Georgia.htm" class="partner-item wow zoomIn a-delay-3" data-wow-delay="1.5s" target="blank"><img src="img/haidelberg.png" alt="Haidelberg Beton"></a>
      </div>
    </div>
  </div>
</section>

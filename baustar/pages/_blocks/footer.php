<section id="footer">
  <div class="footer">
    <div class="small block">
      <div class="wide container">
        <div class="footer-social clearfix">
          <p class="pull-left">&copy; 2017 <a href="/">BAU STAR</a> <?php echo $word['_w41']; ?></p>
          <p class="pull-right"><?php echo $word['_w42']; ?> <a href="http://dev.bunny.ge" class="bunny" target="blank">BUNNY.GE</a><?php echo $word['_w43']; ?></p>
        </div>
      </div>
    </div>
  </div>
</section>

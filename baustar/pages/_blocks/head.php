<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!--google and search engines-->
<meta name="description" content="Baustar.ge - Construction Development Company">

<!--facebook-->
<meta property="og:title" content="Baustar.ge" />
<meta property="og:type" content="website" />
<meta property="og:image" content="/img/_logo-full-big.png" />
<meta property="og:description" content="Baustar.ge - Construction Development Company" />

<link type="image/x-icon" rel="shortcut icon" href="/favicon.ico">
<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="/css/style.css?v=3">
<?php  if( Engine::$slashes[0] == 'varketili-2'|| Engine::$slashes[0] == 'digomi-green-town'|| Engine::$slashes[0] == 'gallery' ) { ?>
<link rel="stylesheet" type="text/css" href="/css/magnific-popup.css">
<?php } ?>

<title>BAU Star</title>

<div class="margin-bottom-20 clearfix">
  <select class="calc calc-select" id="calc-type">
    <option value="670" selected><?php echo $word['_w24']; ?> $670</option>
    <option value="710"><?php echo $word['_w25']; ?> $710</option>
  </select>

  <i class="fa fa-times fa-lg calc-symbol"></i>

  <select class="calc calc-select" id="calc-area">
    <option value="1" selected disabled><?php echo $word['_w27']; ?></option>
    <?php
      $q = $mysqli->query("SELECT id,area FROM flat WHERE floor = {$floor} AND available = 1;");
      while ($s = $q->fetch_assoc()) {
        echo "<option value=\"{$s['id']}\">{$s['area']} კვ.მ</option>";
      }
     ?>
  </select>

  <span class="calc-symbol">=</span>

  <p class="calc"><i class="fa fa-usd"></i> <span id="calc-target">0</span></p>

  <div class="calc calc-right calc-floors js-self-active">
    <span><?php echo $word['_w26']; ?> <strong class="text-green"><?php echo $floor; ?></strong> <i class="fa fa-caret-down"></i></span>
    <ul class="floor-dropdown clearfix">
      <?php
        $q = $mysqli->query("SELECT id FROM floor WHERE flats > 0;");
        while ($s = $q->fetch_assoc()) {
          echo "<li><a href=\"/digomi-green-town/{$s['id']}\">{$s['id']}</a></li>";
        }
       ?>
    </ul>
  </div>
</div>

<div class="sv-floor margin-bottom-20">
  <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="737.039px" height="516.027px" viewBox="0 0 737.039 516.027" enable-background="new 0 0 737.039 516.027" xml:space="preserve" class="building-svg">
    <style>
    	.sv-room {transition: fill .2s ease;fill: transparent;fill-opacity: .85;cursor: pointer;}
      .sv-room.sold {fill: #fff; cursor: default;}
      .sv-room.reserved {fill: #fff; cursor: default;}
    	.sv-room.active {fill: #fff;}
    </style>
    <image overflow="visible" enable-background="new" width="1474" height="1032" xlink:href="<?php echo $f[1]; ?>"  transform="matrix(0.5 0 0 0.5 0 4.882813e-004)"></image>
    <?php foreach ($f[0] as $s) {
      $_s='';
      switch ($s['available']) {
        case '1':$_s='available';break;
        case '-1':$_s='reserved';break;
        default:$_s='sold';break;
      }
      ?>
      <?php echo $s['available']==1? "<a xlink:href=\"/digomi-green-town/{$floor}/{$s['id']}\">":''; ?><path class="sv-room js-room <?php echo $_s; ?>" data-for="<?php echo $s['id']; ?>" data-square="<?php echo $s['area']; ?>" d="<?php echo $s['svg']; ?>"/><?php echo $s['available']==1? "</a>":''; ?>
    <?php } ?>
  </svg>
</div>

<div class="clearfix  margin-bottom-30">
  <a href="/plan/<?php echo $floor; ?>.pdf" class="calc calc-right" style="cursor:pointer;" target="blank"><i class="fa fa-file-pdf-o"></i> <?php echo $word['_w61']; ?></a>
</div>

<li class="header-menu-item"><a href="/"><i class="fa fa-home fa-lg"></i></a></li>
<li class="header-menu-item"><a href="/projects"><?php echo $word['_w1']; ?></a></li>
<li class="header-menu-item"><a href="/about"><?php echo $word['_w2']; ?></a></li>
<li class="header-menu-item"><a href="/contact"><?php echo $word['_w50']; ?></a></li>
<li class="header-menu-item"><a href="?lang=<?php echo $lang->env == 'ka' ? 'en">EN' : 'ka">KA'; ?></a></li>
<li class="header-menu-item"><a href="https://www.facebook.com/baustarBS/" target="blank"><i class="fa fa-facebook-official fa-lg" style="color: #3b5998;"></i></a></li>

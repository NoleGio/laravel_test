<section id="contact">
  <div class="contact">
    <div class="block">
      <div class="wide container">
        <h1 class="wow fadeInUp text-center"><?php echo $word['_w19']; ?></h1>
        <div class="divider"></div>
        <div class="row">
          <div class="col md--40">
            <div class="boxer sm-mb-20 for-text wow fadeInLeft">
              <ul class="list contact-list">
                <li>
                  <i class="fa fa-map-marker fa-lg"></i>
                  <p><?php echo $word['_w8']; ?></p>
                </li>
                <li>
                  <i class="fa fa-phone fa-lg"></i>
                  <p>(+995) 596 04 09 09</p>
                  <p>(+995) 591 37 32 32</p>
                </li>
                <li>
                  <i class="fa fa-at fa-lg"></i>
                  <p><a href="mailto:info@baustar.ge">info@baustar.ge</a></p>
                  <p><a href="mailto:baustarimm@gmail.com">baustarimm@gmail.com</a></p>
                </li>
              </ul>
              <div class="fb-page" data-href="https://www.facebook.com/baustarBS/" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/baustarBS/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/baustarBS/">Bau Star</a></blockquote></div>
            </div>
          </div>
          <div class="col md--60">
            <div class="boxer wow fadeInRight">
              <form action="#" method="post" id="contact-form" name="contact">
                <div class="row">
                  <div class="col md-6">
                    <div class="form-item">
                      <input type="text" placeholder="<?php echo $word['_w20']; ?>" class="textbox" name="name">
                    </div>
                  </div>
                  <div class="col md-6">
                    <div class="form-item">
                      <input type="text" placeholder="<?php echo $word['_w21']; ?>" class="textbox" name="email">
                    </div>
                  </div>
                </div>
                <div class="form-item">
                  <div>
                    <textarea rows="7" placeholder="<?php echo $word['_w22']; ?>" class="textbox" name="message"></textarea>
                  </div>
                  <div id="alertbox"></div>
                </div>
                <div class="text-right">
                  <input type="submit" value="<?php echo $word['_w23']; ?>" class="button">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

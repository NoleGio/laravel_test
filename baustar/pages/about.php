<!-- BAU Star website designed by Bunny.ge -->
<!DOCTYPE html>
<html>
<head>
	<?php require_once '_blocks/head.php'; ?>
</head>
<body class="animated fadeIn fixed-heading">
	<section id="header">
		<div class="scrolled header">
			<div class="wide container">
				<div class="header-content clearfix">
						<a href="/" class="header-logo js-header-logo"><img src="img/baustar-h.png" alt=""></a>
						<ul class="header-menu clearfix animated fadeIn">
							<?php require_once '_blocks/header-menu.php'; ?>
						</ul>
						<div class="nav-btn js-nav-btn animated fadeIn"><span></span><span></span><span></span><span></span></div>
				</div>
			</div>
		</div>
	</section>

	<section id="heading">
		<div class="heading parallax" data-width="1600" data-height="900" data-deltap="50">
			<div class="counters-blur">
				<div class="block-t">
					<div class="wide container">
						<div class="block-title">
							<h1 class="animated fadeInRight"><?php echo $word['_w17']; ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="about-us">
		<div class="about-us">
			<div class="block">
				<div class="wide container">
					<div class="boxer">
						<div class="row">
							<div class="col md--40">
								<img src="https://scontent-fra3-1.xx.fbcdn.net/v/t1.0-9/13139341_1164083893624958_5425789228930080537_n.jpg?oh=255afc63e059040879e4192d4f7a38ee&oe=596FF424" class="responsive img wow fadeInLeft">
							</div>
							<div class="col md--60">
								<div class="text-justify wow fadeIn">
									<p class="margin-bottom-10">
										<?php echo $word['_w47']; ?>
									</p>
									<p class="margin-bottom-10">
									<?php echo $word['_w48']; ?>
									</p>
									<p class="margin-bottom-10"><?php echo $word['_w49']; ?> :</p>
									<ul>
										<li><?php echo $word['_w51']; ?></li>
										<li><?php echo $word['_w52']; ?></li>
										<li><?php echo $word['_w53']; ?></li>
										<li><?php echo $word['_w54']; ?></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="text-center margin-top-30">
						<a href="/projects" class="button wow fadeInUp"><?php echo $word['_w5']; ?></a>
						<a href="/contact" class="button wow fadeInUp" data-wow-delay=".5s"><?php echo $word['_w50']; ?></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php require_once '_blocks/phone.php'; ?>

	<?php require_once '_blocks/contact.php'; ?>

	<section id="location"><div id="map" class="google-map" data-lat="41.762529" data-lng="44.776768" data-icon=""></div></section>

	<?php require_once '_blocks/partners.php'; ?>

	<?php require_once '_blocks/footer.php'; ?>

	<?php require_once '_blocks/scripts.php'; ?>
</body>
</html>

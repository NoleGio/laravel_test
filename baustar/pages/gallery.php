<!-- BAU Star website designed by Bunny.ge -->
<!DOCTYPE html>
<html>
<head>
	<?php require_once __DIR__.'/_blocks/head.php'; ?>
</head>
<body class="animated fadeIn fixed-heading">
	<section id="header">
		<div class="scrolled header">
			<div class="wide container">
				<div class="header-content clearfix">
						<a href="/" class="header-logo"><img src="/img/baustar-h.png" alt=""></a>
						<ul class="header-menu clearfix animated fadeIn">
							<?php require_once __DIR__.'/_blocks/header-menu.php'; ?>
						</ul>
						<div class="nav-btn js-nav-btn animated fadeIn"><span></span><span></span><span></span><span></span></div>
				</div>
			</div>
		</div>
	</section>

	<section id="heading">
		<div class="heading parallax" data-width="1600" data-height="900" data-deltap="50">
			<div class="counters-blur">
				<div class="block-t">
					<div class="wide container">
						<div class="block-title">
							<h1 class="animated fadeInRight"><?php echo $word['_w55']; ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section id="project">
		<div class="block">
			<div class="wide container">

				<div class="row margin-top-20 js-lightbox">

					<div class="col md-4">
						<div class="gallery-item" href="/img/house.jpg">
							<div class="galley-item-inner">
								<img src="/img/house.jpg" class="responsive img gallery-item-img" alt="">
								<div class="gallery-item-cover">
									<div class="fs-table">
										<div class="fs-row">
											<div class="fs-col">
												<div class="fs-item">
													<i class="fa fa-search-plus fa-lg"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col md-4">
						<div class="gallery-item" href="/img/slider/images/2.jpg">
							<div class="galley-item-inner">
								<img src="/img/slider/images/2.jpg" class="responsive img gallery-item-img" alt="">
								<div class="gallery-item-cover">
									<div class="fs-table">
										<div class="fs-row">
											<div class="fs-col">
												<div class="fs-item">
													<i class="fa fa-search-plus fa-lg"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col md-4">
						<div class="gallery-item" href="/img/digomi-2.jpg">
							<div class="galley-item-inner">
								<img src="/img/digomi-2.jpg" class="responsive img gallery-item-img" alt="">
								<div class="gallery-item-cover">
									<div class="fs-table">
										<div class="fs-row">
											<div class="fs-col">
												<div class="fs-item">
													<i class="fa fa-search-plus fa-lg"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col md-4">
						<div class="gallery-item" href="/img/digomi-3.jpg">
							<div class="galley-item-inner">
								<img src="/img/digomi-3.jpg" class="responsive img gallery-item-img" alt="">
								<div class="gallery-item-cover">
									<div class="fs-table">
										<div class="fs-row">
											<div class="fs-col">
												<div class="fs-item">
													<i class="fa fa-search-plus fa-lg"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col md-4">
						<div class="gallery-item" href="/img/digomi-4.jpg">
							<div class="galley-item-inner">
								<img src="/img/digomi-4.jpg" class="responsive img gallery-item-img" alt="">
								<div class="gallery-item-cover">
									<div class="fs-table">
										<div class="fs-row">
											<div class="fs-col">
												<div class="fs-item">
													<i class="fa fa-search-plus fa-lg"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col md-4">
						<div class="gallery-item" href="/img/digomi-5.jpg">
							<div class="galley-item-inner">
								<img src="/img/digomi-5.jpg" class="responsive img gallery-item-img" alt="">
								<div class="gallery-item-cover">
									<div class="fs-table">
										<div class="fs-row">
											<div class="fs-col">
												<div class="fs-item">
													<i class="fa fa-search-plus fa-lg"></i>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div> <!-- row -->

			</div> <!-- wide container -->
		</div>
	</section>

	<?php require_once __DIR__.'/_blocks/phone.php'; ?>

	<?php require_once __DIR__.'/_blocks/contact.php'; ?>

	<section id="location"><div id="map" class="google-map" data-lat="41.762529" data-lng="44.776768" data-icon=""></div></section>

	<?php require_once '_blocks/partners.php'; ?>

	<?php require_once __DIR__.'/_blocks/footer.php'; ?>

	<?php require_once __DIR__.'/_blocks/scripts.php'; ?>
</body>
</html>

<?php
class Engine
{
	public static $slashes = array();

	public $pages = array(
		'home' => array(),
		'uups' => array(),
		'404' => array(),
		'projects' => array(),
		'about' => array(),
		'contact' => array(),
		'gallery' => array(),

		'varketili-1' => array(),
		'varketili-2' => array(),
		'digomi-green-town' => array(
			'__dynamic__' => array(
				'__dynamic__' => array()
			)
		)
	);

	public function validateURL(){

		//cutting REQUEST_URI into array
		$uri = $_SERVER['REQUEST_URI'];
		$slashes = &static::$slashes;

		$qPos = strpos($uri, '?');
		if( $qPos !== false )
		{
			$uri = substr($uri, 0, $qPos);
		}

		if( $uri == '/' )
		{
			$slashes[] =  'home';
		}
		else
		{
			if(strpos($uri,'//')===false)
			{
				$uri = trim($uri,'/');
				$uri = strtolower($uri);
				$slashes = explode('/',$uri);
			}
			else
			{
				static::redirect404();
			}
		}

		//now, validate
		$numSlashes = sizeof($slashes);

		$pg = &$this->pages;
		for( $i = 0; $i < $numSlashes; $i++ )
		{
			if( isset( $pg[$slashes[$i]] ) )
			{
				if( empty( $pg[$slashes[$i]] ) )
				{
					if( $i !== $numSlashes-1 )
					{
						static::redirect404();
					}
				}
				else
				{
					$pg = $pg[$slashes[$i]];
				}
			}
			elseif( isset( $pg['__dynamic__'] ) )
			{
				$pg = $pg['__dynamic__'];
			}
			else
			{
				static::redirect404();
			}
		}
	}

	public function initPage()
	{
		return $_SERVER['DOCUMENT_ROOT']."/pages/".static::$slashes[0].".php";
	}

	public static function redirect404()
	{
		header("Location: /404");
		exit();
	}

}
?>

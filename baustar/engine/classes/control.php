<?php
class Cn{
	public $depth = 1;

	public function initFlat($floor,$flat)
	{
		global $mysqli;
		$this->depth = 3;

		if( preg_match('/^[0-9]+$/',$floor) && preg_match('/^[0-9]+$/',$flat) )
		{
			$floor = (int)$floor;
			$flat = (int)$flat;
			$flatInfo = $mysqli->query("SELECT * FROM flat WHERE id={$flat} AND floor={$floor} LIMIT 1;")->fetch_assoc();

			$rooms = [];

			if( !empty($flatInfo['area_rooms']) )
			{
				$rooms = explode(',',$flatInfo['area_rooms']);
			}
			return [$rooms,$flatInfo];
		}
		else
		{
			Engine::redirect404();
		}
	}

	public function initFloor($floor)
	{
		global $mysqli;
		$this->depth = 2;
		if( preg_match('/^[0-9]+$/',$floor) )
		{
			$floor = (int)$floor;
			if( $floor > 0 && $floor < 16 )
			{
				$floorImg = $mysqli->query("SELECT * FROM floor WHERE id={$floor} LIMIT 1;")->fetch_assoc()['img'];

				$f = [];
				$flats = $mysqli->query("SELECT * FROM flat WHERE floor={$floor};");
				while( $flat = $flats->fetch_assoc() )
				{
					$f[] = $flat;
				}
				return [$f,$floorImg];
			}
			else
			{
				Engine::redirect404();
			}
		}
		else {
			Engine::redirect404();
		}
	}
	public function initBlock()
	{
		global $mysqli;
		$floors = $mysqli->query("SELECT id,flats FROM floor;");
		$f = [];
		while($floor = $floors->fetch_assoc())
		{
			$f[ $floor['id'] ] = (int)$floor['flats'];
		}
		return $f;
	}
}
?>
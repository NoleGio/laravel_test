<?php
class Lang
{
  public $env = 'ka';

  public function detect()
  {
    return __DIR__."/../lang/".$this->env.".php";
  }

  public function __construct()
  {
    if ( isset($_GET['lang']) )
    {
      if( $_GET['lang']=='en' || $_GET['lang'] == 'ka' )
      {
        $this->env = $_GET['lang'];
        setcookie("lang", $_GET['lang'], time()+2592000, "/");
      }
    }
    elseif( isset($_COOKIE['lang']) )
    {
      if( $_COOKIE['lang'] == 'en' || $_COOKIE['lang'] == 'ka' )
      {
        $this->env = $_COOKIE['lang'];
      }
      else
      {
        setcookie("lang", "", time()-3600, "/");
      }
    }
  }
}
?>

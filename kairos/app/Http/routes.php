<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//return view from routes.php
// Route::get('/gio',function(){

// 	return view('frontend.firstpage');

// });

//use controller to return view
// Route::get('/showinfo','newcontroller@returnView');



Route::get('/createUser/{name}/{password}','databaseController@putUser');


Route::post('showinfo','databaseController@showinfo')->name("showinfo");
Route::get('/blax','databaseController@blax');


Route::get('/sometest','databaseController@sometest');
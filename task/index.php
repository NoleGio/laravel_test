<?php
require_once("admin/config.php");





$sql = "SELECT * FROM items";
$result = $con->query($sql);







?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
    <title>Voli.G</title>
    <link rel="stylesheet" href="style.css"/>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
    
    <script src="script.js"></script>
    
</head>
<body>
    <section class="main">
        <div class="ui grid">
            <div class="six column row">
            <?php
                while($row = $result->fetch_assoc()) {
                    
                ?>
                <div class="column">
                    <!-- es aris kubiki, unikaluri identifikatori aris id-->
                    <div class="ui segment quadratic" id="<?php echo $row['id'];?>">
                        
                        <!--es aris imistvis ro yvelas qondes tavisi background image-->
                        <script>
                            $('#<?php echo $row["id"];?>').css("background-image", "url(admin/images/<?php echo $row['image'];?>)");  
                        </script>
                        <!-- $('#1_item').css("background-image", "url(/img.jpg)");   -->

                        <h3><?php echo $row['name'];?></h3>
                        
                    </div>
                    
                    <!--es aris ro daacher rac amovardeba is komponenti unikaluri
                    identifikatori aris id-->
                    <div class="ui modal" id="item_<?php echo $row['id'];?>">
                        <i class="close icon"></i>
                          <div class="header">
                              <?php echo $row['name'];?>
                          </div>
                          <div class="image content">
                            <div class="ui medium image">
                                <img src="admin/images/<?php echo $row['image'];?>">
                            </div>
                            <div class="description">
                                <div class="ui header"><?php echo $row['name'];?></div>
                                <p><?php echo $row['description'];?>
                                <p><a href="https://www.ramelinki.com" target="_blank">Click To Contact Me</a> </p>
                            </div>
                          </div>
                        </div>
                    
                    <!--es aris imistvis rom modali kubikis clickze gichvenos-->
                    <script>
                        $( "#<?php echo $row['id'];?>" ).click(function() {
                            $('#item_<?php echo $row["id"];?>').modal('show');
                        });
                    </script>

                </div>

                <?php } ?>
                
                
                
            </div>
        </div>
    </section>
</body>
</html>
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ProductsController@index');


//i could also use resources but as time is 60 minutes, i didn't go that way.

Route::post('insertproduct','ProductsController@insertProduct');
Route::post('getAllProducts','ProductsController@getAllProducts');
Route::get('edit/{edit}','ProductsController@editProduct');
Route::post('updateproduct','ProductsController@updateProduct');
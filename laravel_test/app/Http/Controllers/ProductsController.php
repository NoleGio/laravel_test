<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use Carbon\Carbon;
class ProductsController extends Controller
{
    //


    public function index(){
    	$products = Product::orderBy('created_at', 'desc')->get();
    	return view('allproducts',compact('products'));
    }

    public function insertProduct(Request $request){
		$product = new Product();
		$product->product_name = $request->name;
		$product->quantity = $request->quantity;
		$product->price = $request->price;
		$insert_time = Carbon::now();
		$product->created_at = $insert_time;
		$product->updated_at = $insert_time;
		if($product->save()){
			return response()->json(['status'=>"200"]);
		}
		else{
			return response()->json(['status'=>"400"]);
		}


    }

    public function editProduct($id){
    	$product = Product::findOrFail($id);
    	return view('edit_product',compact('product'));
    }

    public function updateProduct(Request $request){
    	$product = Product::findOrFail($request->id);
    	if($product){
	    	$product->product_name = $request->name;
			$product->quantity = $request->quantity;
			$product->price = $request->price;
			$insert_time = Carbon::now();
			$product->created_at = $insert_time;
			$product->updated_at = $insert_time;
			if($product->save()){
				return response()->json(['status'=>"200",'method'=>'update']);
			}
			else{
				return response()->json(['status'=>"400"]);
			}
    	}

    }


    public function getAllProducts(){
    	$products = Product::orderBy('created_at', 'desc')->get();
    	return compact('products');
    }
}



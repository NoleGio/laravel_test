<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Update Product</h2>
  <p>Update new Product In Database:</p>
 
    <div class="form-group">
      <label for="usr">Product Name:</label>
      <input type="text" class="form-control" value="{{$product->product_name}}" id="product_name">
    </div>
    <div class="form-group">
      <label for="pwd">Quantity:</label>
      <input type="text" class="form-control"  value="{{$product->quantity}}" id="quantity">
    </div>

    <div class="form-group">
      <label for="pwd">Price Per Item:</label>
      <input type="text" class="form-control" value="{{$product->price}}" id="price">
    </div>

    <input type="hidden" value="{{$product->id}}" id="hiddenproduct">

    <button type="submit" id="update">Insert</button>
  
</div>





<script>

$(document).ready(function(){
	  $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });
                

  var homeURL = "{{url('')}}";
	 function showAllProducts(){
			$.ajax({
				method: "POST",
				url: "{{url('getAllProducts')}}"
			}) 
			.done(function (response){
					
				var allproducts = "";
				for(var i=0;i<response.products.length;i++){
					var eachproduct = "<tr>";
					eachproduct+= "<td>"+response.products[i].product_name+"</td>";
					eachproduct+= "<td>"+response.products[i].quantity+"</td>";
					eachproduct+= "<td>"+response.products[i].price+"</td>";
					eachproduct+= "<td>"+response.products[i].quantity*response.products[i].price+"</td>";
					eachproduct+="<td>"+"<a href=edit/"+response.products[i].id+" class='btn btn-warning'>Edit</a>";
					eachproduct+="</tr>";
					allproducts+=eachproduct;
				}
				$('.products').html(allproducts);
			});
	}



	$('#submit').on('click',function(){
		var name = $('#product_name').val();
		var quantity = $('#quantity').val();
		var price = $('#price').val();

			$.ajax({
				method: "POST",
				url: "{{url('insertproduct')}}",
				data:{name:name,quantity:quantity,price:price},
			}) 
			.done(function (response){
				if(response.status==200){
					alert('inserted');
					showAllProducts();
				}
			});
	});


  $('#update').on('click',function(){
      var name = $('#product_name').val();
      var quantity = $('#quantity').val();
      var price = $('#price').val();
      var id = $('#hiddenproduct').val();

        $.ajax({
          method: "POST",
          url: "{{url('updateproduct')}}",
          data:{name:name,quantity:quantity,price:price,id:id},
        }) 
        .done(function (response){
            if(response.status==200 && response.method=="update"){
              alert("UPDATED SUCCESSFULLY");
              window.location.href=homeURL;
            }
        });

  });


	

});



</script>
</body>
</html>

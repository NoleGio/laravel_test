<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Save Products</h2>
  <p>Save new Product In Database:</p>
 
    <div class="form-group">
      <label for="usr">Product Name:</label>
      <input type="text" class="form-control" id="product_name">
    </div>
    <div class="form-group">
      <label for="pwd">Quantity:</label>
      <input type="text" class="form-control"  id="quantity">
    </div>

    <div class="form-group">
      <label for="pwd">Price Per Item:</label>
      <input type="text" class="form-control"  id="price">
    </div>

    <button type="submit" id="submit">Insert</button>
  
</div>

<div class="container">
  <h2>ALL PRODUCTS</h2>
  <p></p>            
  <table class="table product_table">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Price Per Item</th>
        <th>Total Value Number</th>
        <th>Edit</th>
        
      </tr>
    </thead>
    <tbody class="products">
    @php $total=0;@endphp
    @foreach($products as $product)
    	@php $total+=$product->price*$product->quantity;@endphp
      <tr>
        <td>{{$product->product_name}}</td>
        <td>{{$product->quantity}}</td>
        <td>{{$product->price}}</td>
        <td>{{$product->price*$product->quantity}}</td>
        <td><a href="edit/<?php echo $product->id;?>" type="button" class="btn btn-warning">Edit</a></td>
      </tr>
     @endforeach
     
    
     
    </tbody>
    <td class="total">This is the total value <span class="total_value">{{$total}}</span></td>
  </table>
</div>




<script>

$(document).ready(function(){
	  $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });
                


	 function showAllProducts(){
			$.ajax({
				method: "POST",
				url: "{{url('getAllProducts')}}"
			}) 
			.done(function (response){
					
				var allproducts = "";
				var total=0;
				for(var i=0;i<response.products.length;i++){
					total+=response.products[i].quantity*response.products[i].price;
					var eachproduct = "<tr>";
					eachproduct+= "<td>"+response.products[i].product_name+"</td>";
					eachproduct+= "<td>"+response.products[i].quantity+"</td>";
					eachproduct+= "<td>"+response.products[i].price+"</td>";
					eachproduct+= "<td>"+response.products[i].quantity*response.products[i].price+"</td>";
					eachproduct+="<td>"+"<a href=edit/"+response.products[i].id+" class='btn btn-warning'>Edit</a>";
					eachproduct+="</tr>";
					allproducts+=eachproduct;
				}
				$('.products').html(allproducts);
				$('.total_value').html(total);

			});
	}



	$('#submit').on('click',function(){
		var name = $('#product_name').val();
		var quantity = $('#quantity').val();
		var price = $('#price').val();

			$.ajax({
				method: "POST",
				url: "{{url('insertproduct')}}",
				data:{name:name,quantity:quantity,price:price},
			}) 
			.done(function (response){
				if(response.status==200){
					alert('inserted');
					showAllProducts();
				}
			});
	});


	

});



</script>
</body>
</html>
